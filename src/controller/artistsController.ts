import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import passport from "passport";
import { Artist } from "../entity/Artist";
import { Album } from "../entity/Album";


export const artistsRoute = Router()


artistsRoute.post('/add', passport.authenticate('jwt', { session: false }), async (req, res) => {
	if (req.user.role === "admin") {

		try {
			const exists = await getRepository(Artist).findOne({ where: [{ name: req.body.name }, { spotify_id: req.body.spotify_id }], relations: ["albums"] })
			if (exists) {
				const artist = await getRepository(Artist).save({ ...req.body, artist_id: exists.artist_id, albums: [...exists.albums, ...req.body.albums] })
				if (artist) {
					return res.status(201).json(artist)
				} else {
					return res.status(422).json({ error: "something's wrong with the payload" })
				}
			}

			const artist = await getRepository(Artist).save(req.body)

			if (artist) {
				return res.status(201).json(artist)
			} else {
				return res.status(422).json({ error: "something's wrong with the payload" })
			}

		} catch (error) {
			console.log(error);
			return res.status(500).json(error);
		}
	} return res.status(401).json({ error: "You don't have the permission to do this" })
})

artistsRoute.post('/:id/addSub', passport.authenticate('jwt', { session: false }), async (req, res) => {
	if (req.user.role === "admin") {

		try {
			if (req.params.id) {

				const exists = await getRepository(Artist).findOne({ where: [{ spotify_id: req.body.spotify_id }], relations: ["albums"] })
				if (exists) {
					const existingArtist = await getRepository(Artist).save({ ...req.body, albums: [...exists.albums, ...req.body.albums] })
					if (existingArtist) {
						return res.status(201).json(existingArtist)
					} else {
						return res.status(422).json({ error: "something's wrong with the payload" })
					}
				}
				const mainArtist = await getRepository(Artist).findOne({ artist_id: req.params.id })
				if (mainArtist) {
					const artist = await getRepository(Artist).save({ ...req.body, group: mainArtist })

					if (artist) {
						return res.status(201).json(artist)
					} else {
						return res.status(422).json({ error: "something's wrong with the payload" })
					}

				} return res.status(404).json({ error: "not found" })

			}

		} catch (error) {
			console.log(error);
			return res.status(500).json(error);
		}
	} return res.status(401).json({ error: "You don't have the permission to do this" })
})

artistsRoute.get('/albums', passport.authenticate('jwt', { session: false }), async (req, res) => {
	try {
		if (req.query.spotify_id) {
			const albums = await getRepository(Album)
			.createQueryBuilder("a")
			.select("a.spotify_id")
			.leftJoinAndSelect('artist', 'ar', 'ar.spotify_id')
			.where('ar.spotify_id = :id')
			.andWhere('a.artist_id = ar.artist_id')
			.setParameter('id',req.query.spotify_id )
			.getMany()
			if (albums) {
				return res.json(albums.map((item) => { return item.spotify_id }))
			} return res.status(404).json({ error: "not found" })
		}
	} catch (error) {
		console.log(error);
		return res.status(500).json(error);

	}
})

artistsRoute.get('/getAll', async (req, res) => {
	try {
		const list = await getRepository(Artist).find({ relations: ['sub_units', 'group'] })
		if (list) {
			return res.json(list)
		} else {
			return res.status(404).json({ error: "not found" })
		}

	} catch (error) {
		console.log(error);
		return res.status(500).json(error);

	}
})

artistsRoute.get('/get/:id', async (req, res) => {
	try {
		const artist = await getRepository(Artist).findOne({ spotify_id: req.params.id }, { relations: ["sub_units", 'albums'] })
		if (artist) {
			return res.json(artist)
		} else {
			return res.status(404).json({ error: "not found" })
		}

	} catch (error) {
		console.log(error);
		return res.status(500).json(error);

	}
})