import { Router } from "express";
import multer from "multer";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import { createAvatar } from "../utils/uploader";
import bcrypt from 'bcrypt'
import passport from "passport";
import { generateRefreshToken, generateToken } from "../utils/token";
import { validatorUser } from "../utils/Validator/user-validator";
import jwt from 'jsonwebtoken';

export const userRoute = Router()

userRoute.post('/refreshToken', (req, res) => {

	const authHeader = req.headers['authorization']
	const token = authHeader && authHeader.split(' ')[1]

	if (token == null) return res.sendStatus(401)

	jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, async (err, user) => {
		if (err) {
			return res.sendStatus(401)
		}

		const exists = await getRepository(User).findOne({ where: [{ user_id: user.user_id }] });

		if (exists) {
			delete user.iat;
			delete user.exp;
			const refreshedAccessToken = generateToken({
				email: exists.email,
				username: exists.username,
				user_id: exists.user_id,
				role: exists.role
			})
			const newRefreshToken = Date.now() / 1000 > user.exp - 10000000 ? generateRefreshToken({
				user_id: exists.user_id
			}) : undefined
			return res.json({
				token: refreshedAccessToken,
				refresh_token: newRefreshToken ?? null
			});
		}
		return res.status(401).json({ error: 'Invalid token' });

	});
});


userRoute.get('/acc', passport.authenticate('jwt', { session: false }), async (req, res) => {

	const user = await getRepository(User).findOneOrFail(req.user.user_id);


	if (user) {
		return res.json(user)
	} else {
		return res.status(404).json({ error: "User not found" })
	}
})

userRoute.post('/register', async (req, res) => {

	try {

		let val = validatorUser(req.body);

		if (val != true) {
			return res.status(422).json({ error: val.details[0].message });
		} else {
			const newUser = new User;
			Object.assign(newUser, req.body);
			const exists = await getRepository(User).findOne({ where: [{ email: newUser.email }, { username: newUser.username }] });

			if (exists && exists.username.toLowerCase() === newUser.username.toLowerCase()) {

				res.status(409).json({ error: 'Username already taken' });

				return;
			}


			if (exists && exists.email.toLowerCase() === newUser.email.toLowerCase()) {

				res.status(409).json({ error: 'Email already used' });

				return;
			}
			newUser.role = 'user';
			newUser.password = await bcrypt.hash(newUser.password, 11);

			const savedUser = await getRepository(User).save(newUser);
			res.status(201).json({
				user: newUser,
				token: generateToken({
					email: savedUser.email,
					username: savedUser.username,
					user_id: savedUser.user_id,
					role: savedUser.role
				}),
				refresh_token: generateRefreshToken({
					user_id: savedUser.user_id,
				})
			});

		}
	} catch (error) {
		console.log(error);
		res.status(500).json(error);
	}



})

userRoute.post('/login', async (req, res) => {
	try {
		const user = await getRepository(User).findOne({ email: req.body.email });
		if (user) {
			let samePWD = await bcrypt.compare(req.body.password, user.password)
			if (samePWD) {
				return res.json({
					user,
					token: generateToken({
						user_id: user.user_id,
						username: user.username,
						email: user.email,
						role: user.role
					}),
					refresh_token: generateRefreshToken({
						user_id: user.user_id,
					})
				});
			}
			res.status(401).json({ error: 'Invalid credentials' });
			return
		} res.status(404).json({ error: 'User not found' })
	} catch (error) {
		console.log(error);
		res.status(500).json(error);
	}
});

userRoute.patch('/password', passport.authenticate('jwt', { session: false }), async (req, res) => {
	const user = await getRepository(User).findOne({ email: req.user.email });
	if (user) {
		let samePWD = await bcrypt.compare(req.body.password, user.password)
		if (samePWD) {
			const newPass = await bcrypt.hash(req.body.new_password, 11)
			const passChange = await getRepository(User).save({ ...user, password: newPass })
			if (passChange) {
				return res.status(200).json({ message: "password changed" })
			}
		} return res.status(403).json({ error: "Wrong password" })
	} return res.status(404).json({ error: "User not found" })
})

userRoute.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
	if ((req.params.id === req.user.user_id) || req.user.role === "admin") {
		const deleted = await getRepository(User).delete({user_id: req.params.id})
		if (deleted) {
			return res.status(204).json(deleted)
		}return res.status(404).json({error: "User not found"})
	}
})

