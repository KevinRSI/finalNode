import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import passport from "passport";
import { Song } from "../entity/Song";
import { Ranking_songs } from "../entity/Ranking_Song";

export const ranksRoute = Router()

ranksRoute.post('/songs', passport.authenticate('jwt', { session: false }), async (req, res) => {
	if (req.user.user_id) {
		try {
			const user = await getRepository(User).findOne(req.user.user_id)
			const song = await getRepository(Song).findOne(req.body?.song.song_id, {relations: ['album']})
			const rankExists = await getRepository(Ranking_songs).findOne({ song: req.body?.song, user: { user_id: req.user.user_id } }, { relations: ["user", "song"] })
			if (rankExists && req.body.rank) {
				const rewriteRank = await getRepository(Ranking_songs).save({ ...rankExists, rank: req.body.rank, note: req.body.note ? req.body.note : rankExists.note });
				if (rewriteRank) {
					return res.status(201).json(rewriteRank)
				} return res.status(422).json({ error: "something's wrong with the payload" })
			} else if (user && song && req.body.rank) {
				const sendData = await getRepository(Ranking_songs).save({ user: user, song: song, rank: req.body.rank, note: req.body.note, album: song.album });
				if (sendData) {
					return res.status(201).json(sendData)
				} return res.status(422).json({ error: "something's wrong with the payload" })
			}
			return res.status(404).json({ error: "not found" })
		} catch (error) {
			console.log(error);
			res.status(500).json(error)
		}
	} return res.status(401).json({ error: "not authenticated" })
})

ranksRoute.get('/get/:id', async (req, res) => {
	
	try {		
		if (req.params.id && req.query.u) {
			const ranks = await getRepository(Ranking_songs).find({ where: { user: { username: String(req.query.u) }, album: {spotify_id: req.params.id} }, relations: ['album', 'user', 'song'] })
			
			res.status(200).json(ranks)
		}

	} catch (error) {
		console.log(error);
		res.status(500).json(error)
	}
})