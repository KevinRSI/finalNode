import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import passport from "passport";
import { Artist } from "../entity/Artist";
import { Album } from "../entity/Album";


export const albumsRoute = Router()

albumsRoute.get('/lastReleases', async (req, res) => {
	try {
		const last = await getRepository(Album)
			.createQueryBuilder('a')
			.select('a')
			.limit(20)
			.orderBy('release_date', 'DESC')
			.getMany()

		if (last) {
			return res.status(200).json(last)
		} return res.status(404).json({ error: "not found" })

	} catch (error) {
		console.log(error);
		res.status(500).json(error)

	}
})

albumsRoute.get('/one/:sid', async (req, res) => {
	try {
		if (req.params.sid) {
			const album = await getRepository(Album).findOne({ spotify_id: req.params.sid }, { relations: ["artist"] })
			if (album) {
				return res.status(200).json(album)
			} return res.status(404).json({ error: "not found" })
		} return res.status(400).json({ error: "missing parameter" })
	} catch (error) {
		console.log(error);
		res.status(500).json(error)
	}
})

albumsRoute.get('/likes', passport.authenticate('jwt', { session: false }), async (req, res) => {
	try {		
		const likes = await getRepository(Album).createQueryBuilder('a')
			.leftJoinAndSelect('a.ranks', 'rank')
			.leftJoinAndSelect('rank.user', 'u', 'rank.user_id = :uid')
			.where('rank.album = a.album_id')
			.setParameter("uid", req.user.user_id)
			.getMany()
		if (likes) {
			return res.json(likes)

		} return res.status(404).json({error: "Not found"})

	} catch (error) {
		console.log(error);
		res.status(500).json(error)
	}
})
