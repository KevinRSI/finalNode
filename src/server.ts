import express from 'express';
import cors from 'cors';
import passport from 'passport';
import { userRoute } from './controller/userController';
import { configurePassport } from './utils/token';
import { artistsRoute } from './controller/artistsController';
import { albumsRoute } from './controller/albumsController';
import { ranksRoute } from './controller/ranksController';

export const server = express();
server.use(cors());
configurePassport();
server.use(express.json())
server.use(passport.initialize())


server.use('/api/user', userRoute)
server.use('/api/artist', artistsRoute)
server.use('/api/album', albumsRoute)
server.use('/api/rank', ranksRoute)