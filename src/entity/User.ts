import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Ranking_songs } from "./Ranking_Song";


@Entity()
export class User {
	@PrimaryGeneratedColumn('uuid')
	user_id: string;

	@Column({length: 50})
	username: string;

	@Column()
	email: string;

	@Column()
	password: string;

	@Column({ default: 'user' })
	role: string;

	@Column({ nullable: true })
	avatar: string;

	@CreateDateColumn()
	created_at: Date;

	@OneToMany(() => Ranking_songs, song_rank => song_rank.user, {onDelete: "CASCADE"})
	song_rank: Ranking_songs[];


	toJSON() {
		return {
			user_id: this.user_id,
			username: this.username,
			email: this.email,
			role: this.role,
			avatar: this.avatar ? this.avatar : null,
			created_at: this.created_at,
		}

	}

}
