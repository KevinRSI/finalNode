import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Artist } from "./Artist";
 import { Ranking_songs } from "./Ranking_Song";
import { Song } from "./Song";


@Entity()
export class Album {
	@PrimaryGeneratedColumn('uuid')
	album_id: string;

	@Column()
	title: string;

	@Column()
	total_tracks: number;

	@Column({unique: true})
	spotify_id: string;

	@Column({ nullable: true })
	picture: string;

	@Column({ type: 'date' })
	release_date: Date

	@Column()
	link: string

	@Column()
	album_type: string

	@ManyToOne(() => Artist, artist => artist.albums, {onDelete: "CASCADE", cascade: ["insert"]})
	@JoinColumn({ name: 'artist_id' })
	artist: Artist

	@OneToMany(() => Song, song => song.album, { eager: true, cascade: ['insert'], onDelete: "CASCADE" })
	songs: Song[];

	@OneToMany(() => Ranking_songs, album_rank => album_rank.album)
	ranks: Ranking_songs[];

	toJSON() {
		return {
			...this,
		}

	}

}
