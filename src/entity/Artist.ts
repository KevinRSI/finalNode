import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Album } from "./Album";




@Entity()
export class Artist {
	@PrimaryGeneratedColumn('uuid')
	artist_id: string;

	@Column()
	name: string;

	@Column()
	type: string;

	@Column({unique: true})
	spotify_id: string;

	@Column({ nullable: true })
	picture: string;

	@OneToMany(() => Artist, sub => sub.group)
	sub_units: Artist[];

	@ManyToOne(()=> Artist, artist => artist.sub_units)
	@JoinColumn({name: "group_id"})
	group: Artist
	

	@OneToMany(() => Album, album => album.artist, {cascade: true})
	albums: Album[];

	toJSON() {
		return {
			...this,
		}

	}

}
