import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Album } from "./Album";
import { Song } from "./Song";
import { User } from "./User";


@Entity()
export class Ranking_songs {
	@PrimaryGeneratedColumn('uuid')
	song_rank_id: string;

	@Column()
	rank: string;

	@Column({ type: "text", nullable: true })
	note: string;

	@ManyToOne(() => User, user => user.song_rank, {onDelete: "CASCADE"})
	@JoinColumn({ name: 'user_id' })
	user: User;

	@ManyToOne(() => Song, song => song.ranks)
	@JoinColumn({ name: 'song_id' })
	song: Song;

	@ManyToOne(() => Album, album => album.ranks)
	@JoinColumn({ name: 'album_id' })
	album: Album;

	toJSON() {
		return {
			rank: this.rank,
			note: this.note,
			song_rank_id: this.song_rank_id,
			song: this.song,
			user: this.user.user_id
		}

	}

}
