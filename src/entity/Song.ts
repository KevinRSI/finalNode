import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Album } from "./Album";
import { Ranking_songs } from "./Ranking_Song";


@Entity()
export class Song {
	@PrimaryGeneratedColumn('uuid')
	song_id: string;

	@Column()
	title: string;

	@Column({unique: true})
	spotify_id: string;

	@Column()
	link: string;

	@ManyToOne(() => Album, album => album.songs, {onDelete: "CASCADE"})
	@JoinColumn({ name: 'album_id' })
	album: Album;

	@OneToMany(() => Ranking_songs, song_rank => song_rank.song)
	ranks: Ranking_songs[];

	toJSON() {
		return {
			...this,
		}

	}

}
