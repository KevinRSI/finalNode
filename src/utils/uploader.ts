import multer from 'multer';
import sharp from 'sharp';
import { v4 as uuidv4 } from 'uuid';


export async function createAvatar(file: Express.Multer.File) {

	const thumbnailFolder = __dirname + '/../../public/uploads/avatar/'

	let name = uuidv4() + '.jpeg'
	await sharp(file.buffer,)
		.resize(200, 200, { fit: 'inside' })
		.toFormat('jpeg')
		.jpeg({ quality: 90 })
		.toFile(thumbnailFolder + name)


	return name
}



export const uploader = multer({
	limits: {
		fileSize: 8000000
	}
});
