import Joi from 'joi';

export function validatorUser(data:Object) {
        let schema = Joi.object({
            username: Joi.string()
                .min(1)
                .max(50)
                .required(),

            email: Joi.string()
                .email()
                .required(),

            password: Joi.string()
                .required(),

            avatar: Joi.string()
            .allow(''),

            role: Joi.string(),

        })
    
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error } = schema.validate(data, options);

    if (error) {
        return error;
    } else {
        return true
    }
};

export function validatorPatchUser(data:Object) {
    let schema = Joi.object({
        id: Joi.number()
        .required(),

        username: Joi.string()
            .min(1)
            .max(50).allow(null),

        email: Joi.string()
            .email().allow(null),

        password: Joi.string().allow(null),
        
        address : Joi.string().allow(null),

        avatar: Joi.string().allow(null),

        role: Joi.string(),

    })

const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

const { error } = schema.validate(data, options);

if (error) {
    return error;
} else {
    return true
}
};
