
import jwt from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import 'dotenv-flow/config'
import { getRepository } from 'typeorm';
import { User } from '../entity/User';


export function generateToken(payload) {
    const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: 60 * 60, algorithm: "HS512" });
    return token;
}

export function generateRefreshToken(user) {
	return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '1y', algorithm: "HS512" });
}


export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, async (payload, done) => {
        try {
            const user = await getRepository(User).findOne({ email: payload.email });

            if (user) {
                return done(null, {
                    email: user.email,
                    user_id: user.user_id,
                    role: user.role,
					username: user.username
                });
            }

            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}
